#include "SFML/Network.hpp"
#include "SFML/System.hpp"
#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <chrono>
#include <ctime>
#include "./GameDatatypes.hpp"

// #define debugPL cout << " playerlist: ["; size_t __i_ = 0; \
//                 for (auto& pinfo : playerlist)  { \
//                     if (__i_ == freeIndex) cout << " {{-> "; \
//                     cout << pinfo.name << " " <<  statusIDnames[static_cast<int> (pinfo.status)]; \
//                     if (__i_ == freeIndex) cout << " <-}}"; \
//                     cout << ", "; \
//                     ++__i_; \
//                 } \
//                 cout << "]\n";
#define debugPL ;

// #define debugAccounts for (auto& el: accounts) { \
//                         cout << el.first << " = " << el.second.id << " " << el.second.password << " " \
//                              << el.second.name << " " << el.second.email \
//                              << " " << el.second.date << "\n"; \
//                         }
#define debugAccounts ;

using namespace std;
using namespace sf;

struct userAccount {
    string id, user, password, name, email, date;
};

TcpListener                       listener;
vector<unique_ptr<TcpSocket>>     players;
vector<sf::Clock>                 playersTime;
sf::Clock                         logClock;
vector<int>                       playersEnemy;
vector<playerInfo>                playerlist;
map<string, userAccount>          accounts;
map<string, string>               configs;
size_t                            freeIndex;
size_t                            matchCount;

constexpr int timeout = 30;

void registerAccount (string ulogin, string upassword, string name, string email);
string getTimestamp ();
void disconnectPlayer(size_t pl_id);
void logToFile (string message);
void saveMatch(string match);
string getDatestamp (char sep = '/');

void sendPlayerlist () {
    Packet packet;
    packet << -1 //to all players
    << static_cast<int> (packetID::Playerlist) //msg type
           //payload:
           << static_cast<int> (playerlist.size());

    for (auto& pinfo : playerlist) {
        packet << pinfo.id << pinfo.name 
        << static_cast<int> (pinfo.status);
        packet << static_cast<int> (pinfo.battleOpts.size());
        for (auto& element : pinfo.battleOpts)
            packet << element.first << element.second;
    }

    for (auto& player : players) {
        player->send(packet);
    }
    //cout << "Sended player list!\n";
    debugPL;
}

void sendLoginResponse (int fromPlayer, string ulogin, string upassword) {
    Packet packet;
    auto it = accounts.find (ulogin);
    if (it != accounts.end()) {
        if (it->second.password == upassword) {
            playerlist[fromPlayer].name = it->first;
            playerlist[fromPlayer].status = statusID::online;
            cout << "Successfully Login from player " << fromPlayer << "\n";
            logToFile (playerlist[fromPlayer].name + " logon");
            
            packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << true;
            players[fromPlayer]->send(packet);        
            return;
        } else {
            logToFile (playerlist[fromPlayer].name + " incorrect password");
            packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << false;
            players[fromPlayer]->send(packet);
            return;
        }
    }
    //only goes through here in case of user failure
    logToFile (playerlist[fromPlayer].name + " incorrect user");
    packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << false;
    players[fromPlayer]->send(packet);
}

void sendRegisterResponse (int fromPlayer, string ulogin, string upassword, string uname, string uemail) {
    Packet packet;
    auto it = accounts.find (ulogin);
    if (it == accounts.end()) {
        registerAccount (ulogin, upassword, uname, uemail);
        cout << "Successfully Register from player " << fromPlayer << "\n";
        logToFile (playerlist[fromPlayer].name + " new account");
        packet << fromPlayer << static_cast<int> (packetID::RegisterResponse) << true;
        players[fromPlayer]->send(packet);
                
        return;
    }
    //only goes through here in case of register failure
    packet << fromPlayer << static_cast<int> (packetID::RegisterResponse) << false;
    players[fromPlayer]->send(packet);
}

void sendGameEnd (int fromPlayer, int toPlayer) {
    Packet packet;
    packet << fromPlayer << static_cast<int> (packetID::GameEnd);
    players[toPlayer]->send(packet);
    // if (playerlist[toPlayer].status != statusID::offline)
    //     playerlist[toPlayer].status = statusID::online;
    cout << "Sended GameEnd to " << toPlayer << "\n";   
}

void acceptNewPlayers() {
    //seek free player slot
    if  (playerlist[freeIndex].status != statusID::offline) {

        for (freeIndex = 0; freeIndex < players.size() and \
            playerlist[freeIndex].status != statusID::offline; \
            ++freeIndex);

        if (playerlist[freeIndex].status != statusID::offline) {
            players.emplace_back (new TcpSocket());
            players.back()->setBlocking (false);
            playerlist.emplace_back (static_cast<int>(players.size() - 1), "unknown", statusID::offline);
            playersTime.emplace_back();
            playersEnemy.emplace_back(-1);
            freeIndex = players.size()-1;
        }
    }

    if (listener.accept(*players[freeIndex]) == Socket::Done) {
        cout << "player " << freeIndex << " connected!\n";
        playerlist[freeIndex].status = statusID::unlogged;
        debugPL;
    }
}

void handleClientRequest(Packet packet, int pktID, int fromPlayer, int toPlayer) {
    switch (static_cast<packetID> (pktID)) {
        case packetID::Connect :
            cout << "Estabilishing connection to player " << toPlayer << "...\n";
            if (playerlist[toPlayer].status == statusID::online) {
                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Connect);
                playerlist[fromPlayer].status = playerlist[toPlayer].status = statusID::playing;
                players[toPlayer]->send(packet);
                cout << "Connect from player " << fromPlayer << " to player " << toPlayer << "\n";

                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Turn);
                players[fromPlayer]->send(packet);

                playersEnemy[fromPlayer] = toPlayer;
                playersEnemy[toPlayer] = fromPlayer;
            }
            break;
        case packetID::Login : {
            string ulogin, upassword;
            packet >> ulogin >> upassword;
            sendLoginResponse (fromPlayer, ulogin, upassword);
            break;
        }
        case packetID::Register : {
            string ulogin, upassword, uname, uemail;
            packet >> ulogin >> upassword >> uname >> uemail;
            sendRegisterResponse (fromPlayer, ulogin, upassword, uname, uemail);
            break;
        }
        case packetID::Options : {
            cout << "Options from player " << fromPlayer << "\n";
            auto& bo = playerlist[fromPlayer].battleOpts;
            int mapSize;
            packet >> mapSize;
            std::string key;
            int value;
            for (int i = 0; i < mapSize; ++i) {
                packet >> key >> value;
                bo[key] = value;
            }
            break;
        }
        case packetID::GameEnd : {
            cout << "GameEnd from player " << fromPlayer << "\n";
            playerlist[fromPlayer].status = statusID::online;
            sendGameEnd(fromPlayer, toPlayer);
            break;
        }
        case packetID::MatchWinner : {
            cout << "MatchWinner from player " << fromPlayer << "\n";
            string totalTime;
            int shoots1, shoots2;
            packet >> totalTime >> shoots1 >> shoots2;
            //cout << "{" << playerlist[fromPlayer].name << " " << totalTime << " " << shoots1 << " " << shoots2 << "}\n";
            auto& winner = playerlist[fromPlayer];
            auto& loser  = playerlist[playersEnemy[fromPlayer]];
            ostringstream match;
            match << totalTime << ";" 
                  << winner.id << ";" << winner.name << ";" 
                  << loser.id << ";" << loser.name << ";" 
                  << "battleOptions{";
            for (auto& el : playerlist[fromPlayer].battleOpts) {
                match << el.first << ":" << el.second << ",";
            }
            match << "};";
            saveMatch (match.str());
            break;
        }
        case packetID::Disconnect : {
            disconnectPlayer(fromPlayer);
            cout << "Player " << playerlist[fromPlayer].name << " disconnected\n";
            logToFile (playerlist[fromPlayer].name + " disconected");
            break;   
        }
        case packetID::keepAlive : {
            playersTime[fromPlayer].restart();
            //cout << "Player " << playerlist[fromPlayer].name << " keepAlive\n";
            return; //do not update player list
        }
        default : { //redirect
            if (toPlayer < 0 || toPlayer > players.size() -1) return; 

            //cout << fromPlayer << " ===" << packetIDnames[pktID] << "===> " << toPlayer << "\n";
            players[toPlayer]->send(packet);

            return; //do not update player list
        }

    }
    //Since a new player might have entered, or 2 of them are now playing,
    //it's necessary to send a packet updating the Playerlist;
    sendPlayerlist();

    if (fromPlayer >= 0)
        playersTime[fromPlayer].restart();
}

void disconnectPlayer(size_t pl_id) {
    if (playerlist[pl_id].status == statusID::playing)
        sendGameEnd(pl_id, playersEnemy[pl_id]);

    playerlist[pl_id].status = statusID::offline;
    playersTime[pl_id].restart();
    playersEnemy[pl_id] = -1;
    players[pl_id]->disconnect();    
    freeIndex = pl_id;
    logToFile (playerlist[pl_id].name + " disconected");
}

void checkTimeout() {
    bool changed = false;
    for (int i = 0; i < playersTime.size(); ++i) {
        if ((playersTime[i].getElapsedTime().asSeconds() >= timeout) and (playerlist[i].status != statusID::offline)) {
            disconnectPlayer(i);
            logToFile ( playerlist[i].name + " disconected(time out)");
            changed = true;
        }
    }
    if (changed)
        sendPlayerlist ();
}

void loadAccounts () {
    ifstream input {"." + configs["DataDirectory"] + "users"};
    string id, user, pass, name, email, date, line;

    #define getword(_xyz_) getline(s, _xyz_, ';')

    if (input.is_open()) {
        while (getline (input, line)) {
            istringstream s{line};

            getword(id);
            getword(user);
            getword(pass);
            getword(name);
            getword(email);
            getword(date);

            accounts[user] = userAccount{id, user, pass, name, email, date};
        }
    } else {
        cout << ("." + configs["DataDirectory"] + "users") << " not found\n";
    }

    debugAccounts;
}

void saveAccounts () {
    ofstream output {"." + configs["DataDirectory"] + "users"};
    for (auto& user : accounts)
        output << user.second.id << ";" << user.first << ";" 
               << user.second.password << ";" << user.second.name <<";" 
               << user.second.email << ";"  << user.second.date << ";\n";
    output << std::flush;
}

void registerAccount (string ulogin, string upassword, string uname, string uemail) {
    string uid = to_string(accounts.size());
    accounts[ulogin] = userAccount{uid, ulogin, upassword, uname, uemail, getDatestamp()};
    saveAccounts();
    debugAccounts;
}

void logOnline () {
    ofstream output {"." + configs["LogDirectory"] + "online.log"};
    for (auto& pinfo : playerlist) {
        if (pinfo.status == statusID::online)
            output << pinfo.name << "\n";
        if (pinfo.status == statusID::playing)
            output << pinfo.name << " vs " << playerlist[playersEnemy[pinfo.id]].name << "\n";
    }
    output << std::flush;
}

void logToFile (string message) {
    ofstream output {"." + configs["LogDirectory"] + "admiralmind-" + getDatestamp('-') + ".log", ios::app};
    output << getTimestamp() << " " << message << "\n";
    output << std::flush;
}

void countMatches() {
    ifstream input {"." + configs["DataDirectory"] + "matches"};
    if (input.is_open()) {
        string line;
        while (getline(input, line)) {
            ++matchCount;
        }
    }
}

void saveMatch(string match) {
    //contar o numero de linhas para salvar ID...
    ofstream output {"." + configs["DataDirectory"] + "matches", ios::app};
    output << matchCount << ";" << match << "\n";
    ++matchCount;
    output << std::flush;
}

string getTimestamp () {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    // output << "["
    //        <<  padIt(tm_hour) << ":" << padIt(tm_min) << "|"
    //        <<  padIt(tm_mday)    << "-"  
    //        <<  padIt(tm_mon + 1) << "-" 
    //        <<  now_in.tm_year + 1900 
    //        << "]";
    output << padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec);
    return output.str();
}

string getDatestamp (char sep) {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    // output <<  now_in.tm_year + 1900 << "-" << padIt(tm_mon + 1) << "-" << padIt(tm_mday);
    if (sep=='-') { //saving to a file, reverse order
        output << now_in.tm_year + 1900 << sep << padIt(tm_mon + 1) << sep << padIt(tm_mday);
        return output.str();
    }
    output << padIt(tm_mday) << sep << padIt(tm_mon + 1) << sep << now_in.tm_year + 1900;
    return output.str();
}

vector<string> splitIntoWords(string line) {
    vector<string> words;
    istringstream s(line);
    copy(istream_iterator<string>(s), istream_iterator<string>(), back_inserter(words));
    return words;
}

void loadConf() {
    configs["Port"] = "50986";
    configs["LogDirectory"] = "/log/";
    configs["DataDirectory"] = "/data/";
    ifstream input {"./admiralmindd.conf"};
    if (input.is_open()) {
        string line;
        vector<string> words;
        while (getline(input, line)) {
            if (not line.empty() and line[0] == '#') continue;
            words = splitIntoWords(line);
            if (words.size() > 1) {configs[words[0]] = words[1];}
        }
    };
    countMatches();
}

int main () {
    int toPlayer, fromPlayer, pktID;
    bool working = true;
    Packet packet;
    listener.setBlocking(false);

    cout << "Server started\n";

    loadConf();
    int port = stoi(configs["Port"]);

    for (auto& el : configs) {
        cout << el.first << " = " << el.second << "\n";
    }

    loadAccounts();

    while (listener.listen (port) != Socket::Done);
    cout << "Successfully binded to port " << port << "\n";
    logToFile("System initialized at port " + configs["Port"]);

    //prepare first player slot
    players.emplace_back (new TcpSocket());
    players.back()->setBlocking (false);
    playerlist.emplace_back (static_cast<int>(players.size() - 1), "unknown", statusID::offline);
    playersTime.emplace_back();
    playersEnemy.emplace_back(-1);
    freeIndex = 0;
    debugPL;
    
    logClock.restart();
    while (working) {
        acceptNewPlayers();

        fromPlayer = 0;
        for (auto& player : players) {
            if (player->receive(packet) == Socket::Done) {
                packet >> toPlayer >> pktID;
                if (pktID != static_cast<int>(packetID::keepAlive)) 
                    {cout << "from: " << fromPlayer << " to: " << toPlayer << " id:" << packetIDnames[pktID] << "\n";}
                handleClientRequest(packet, pktID, fromPlayer, toPlayer);
            }
            ++fromPlayer;
        }
        checkTimeout();
        if (logClock.getElapsedTime().asSeconds() > 10) {
            logOnline();
            logClock.restart();
        }

        sleep(seconds(0.2));        
    }

}