string getTimestamp () {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    // output << "["
    //        <<  padIt(tm_hour) << ":" << padIt(tm_min) << "|"
    //        <<  padIt(tm_mday)    << "-"  
    //        <<  padIt(tm_mon + 1) << "-" 
    //        <<  now_in.tm_year + 1900 
    //        << "]";
    output << padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec);
    return output.str();
}

string getDatestamp (char sep) {
    using namespace std::chrono;
    #define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    // output <<  now_in.tm_year + 1900 << "-" << padIt(tm_mon + 1) << "-" << padIt(tm_mday);
    if (sep=='-') { //saving to a file, reverse order
        output << now_in.tm_year + 1900 << sep << padIt(tm_mon + 1) << sep << padIt(tm_mday);
        return output.str();
    }
    output << padIt(tm_mday) << sep << padIt(tm_mon + 1) << sep << now_in.tm_year + 1900;
    return output.str();
}

vector<string> splitIntoWords(string line) {
    vector<string> words;
    istringstream s(line);
    copy(istream_iterator<string>(s), istream_iterator<string>(), back_inserter(words));
    return words;
}