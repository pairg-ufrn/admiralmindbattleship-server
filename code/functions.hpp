
string getTimestamp ();
void logToFile (string message);
void logOnline ();
void countMatches();
void saveMatch(string match);
void loadConf();
void registerAccount (string ulogin, string upassword, string name, string email);
void loadAccounts ();
void saveAccounts ();
string getDatestamp (char sep = '/');
void disconnectPlayer(size_t pl_id, bool timeout = false);
void acceptNewPlayers();
void checkTimeout();

