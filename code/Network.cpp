void sendPlayerlist () {
    Packet packet;
    packet << -1 //to all players
    << static_cast<int> (packetID::Playerlist) //msg type 
    << static_cast<int> (playersOnline); //size
    
    for (auto& pinfo : playerlist) {

        if (pinfo.status != statusID::online and
            pinfo.status != statusID::playing) continue;

        packet << pinfo.id << pinfo.name 
        << static_cast<int> (pinfo.status);
        packet << static_cast<int> (pinfo.battleOpts.size());
        for (auto& element : pinfo.battleOpts)
            packet << element.first << element.second;
    }

    for (auto& player : players) {
        player->send(packet);
    }
    //cout << "Sended player list!\n";
    debugPL;
}

void sendLoginResponse (int fromPlayer, string ulogin, string upassword) {
    Packet packet;
    auto it = accounts.find (ulogin);
    if (it != accounts.end()) {
        if (it->second.password == upassword) {
            playerlist[fromPlayer].name = it->first;
            playerlist[fromPlayer].status = statusID::online;
            ++playersOnline;
            cout << "Successfully Login from player " << fromPlayer << "\n";
            logToFile (playerlist[fromPlayer].name + " logon");
            
            packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << true;
            players[fromPlayer]->send(packet);        
            return;
        } else {
            logToFile (playerlist[fromPlayer].name + " incorrect password");
            packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << false;
            players[fromPlayer]->send(packet);
            return;
        }
    }
    //only goes through here in case of user failure
    logToFile (playerlist[fromPlayer].name + " incorrect user");
    packet << fromPlayer << static_cast<int> (packetID::LoginResponse) << false;
    players[fromPlayer]->send(packet);
}

void sendRegisterResponse (int fromPlayer, string ulogin, string upassword, string uname, string uemail) {
    Packet packet;
    auto it = accounts.find (ulogin);
    if (it == accounts.end()) {
        registerAccount (ulogin, upassword, uname, uemail);
        cout << "Successfully Register from player " << fromPlayer << "\n";
        logToFile (playerlist[fromPlayer].name + " new account");
        packet << fromPlayer << static_cast<int> (packetID::RegisterResponse) << true;
        players[fromPlayer]->send(packet);
                
        return;
    }
    //only goes through here in case of register failure
    packet << fromPlayer << static_cast<int> (packetID::RegisterResponse) << false;
    players[fromPlayer]->send(packet);
}

void sendGameEnd (int fromPlayer, int toPlayer) {
    Packet packet;
    packet << fromPlayer << static_cast<int> (packetID::GameEnd);
    players[toPlayer]->send(packet);
    // if (playerlist[toPlayer].status != statusID::offline)
    //     playerlist[toPlayer].status = statusID::online;
    cout << "Sended GameEnd to " << toPlayer << "\n";   
}

void handleClientRequest(Packet packet, int pktID, int fromPlayer, int toPlayer) {
    switch (static_cast<packetID> (pktID)) {
        case packetID::Challenge :
            if (playerlist[toPlayer].status == statusID::online) {
                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Challenge);
                players[toPlayer]->send(packet);
                cout << fromPlayer << " challenged player " << toPlayer << "\n";
            }
            break;
        case packetID::Refuse :
            if (playerlist[toPlayer].status == statusID::online) {
                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Refuse);
                players[toPlayer]->send(packet);
                cout << fromPlayer << " refused player " << toPlayer << "\n";
            }
            break;
        case packetID::Connect :
            cout << "Estabilishing connection to player " << toPlayer << "...\n";
            if (playerlist[toPlayer].status == statusID::online) {
                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Connect);
                playerlist[fromPlayer].status = playerlist[toPlayer].status = statusID::playing;
                players[toPlayer]->send(packet);
                cout << "Connect from player " << fromPlayer << " to player " << toPlayer << "\n";

                packet.clear();
                packet << fromPlayer << static_cast<int> (packetID::Turn);
                players[fromPlayer]->send(packet);

                playersEnemy[fromPlayer] = toPlayer;
                playersEnemy[toPlayer] = fromPlayer;
            }
            break;
        case packetID::Login : {
            string ulogin, upassword;
            packet >> ulogin >> upassword;
            sendLoginResponse (fromPlayer, ulogin, upassword);
            break;
        }
        case packetID::Register : {
            string ulogin, upassword, uname, uemail;
            packet >> ulogin >> upassword >> uname >> uemail;
            sendRegisterResponse (fromPlayer, ulogin, upassword, uname, uemail);
            break;
        }
        case packetID::Options : {
            cout << "Options from player " << fromPlayer << "\n";
            auto& bo = playerlist[fromPlayer].battleOpts;
            int mapSize;
            packet >> mapSize;
            std::string key;
            int value;
            for (int i = 0; i < mapSize; ++i) {
                packet >> key >> value;
                bo[key] = value;
            }
            break;
        }
        case packetID::GameEnd : {
            cout << "GameEnd from player " << fromPlayer << "\n";
            playerlist[fromPlayer].status = statusID::online;
            sendGameEnd(fromPlayer, toPlayer);
            break;
        }
        case packetID::MatchWinner : {
            cout << "MatchWinner from player " << fromPlayer << "\n";
            string totalTime;
            int shoots1, shoots2;
            packet >> totalTime >> shoots1 >> shoots2;
            //cout << "{" << playerlist[fromPlayer].name << " " << totalTime << " " << shoots1 << " " << shoots2 << "}\n";
            auto& winner = playerlist[fromPlayer];
            auto& loser  = playerlist[playersEnemy[fromPlayer]];
            ostringstream match;
            match << winner.id << ";"  << loser.id << ";"
                  << "match_time=" << totalTime << ";" 
                  << "battleOptions{";
            for (auto& el : playerlist[fromPlayer].battleOpts) {
                match << el.first << ":" << el.second << ",";
            }
            match << "};";
            saveMatch (match.str());
            break;
        }
        case packetID::Disconnect : {
            disconnectPlayer(fromPlayer);
            cout << "Player " << playerlist[fromPlayer].name << " disconnected\n";
            break;   
        }
        case packetID::keepAlive : {
            playersTime[fromPlayer].restart();
            //cout << "Player " << playerlist[fromPlayer].name << " keepAlive\n";
            return; //do not update player list
        }
        default : { //redirect
            if (toPlayer < 0 || toPlayer > players.size() -1) return; 

            //cout << fromPlayer << " ===" << packetIDnames[pktID] << "===> " << toPlayer << "\n";
            players[toPlayer]->send(packet);

            return; //do not update player list
        }

    }
    //Since a new player might have entered, or 2 of them are now playing,
    //it's necessary to send a packet updating the Playerlist;
    sendPlayerlist();

    if (fromPlayer >= 0)
        playersTime[fromPlayer].restart();
}

void start () {
    int toPlayer, fromPlayer, pktID;
    bool working = true;
    Packet packet;
    listener.setBlocking(false);

    cout << "Server started\n";

    loadConf();
    int port = stoi(configs["Port"]);

    for (auto& el : configs) {
        cout << el.first << " = " << el.second << "\n";
    }

    loadAccounts();

    while (listener.listen (port) != Socket::Done);
    cout << "Successfully binded to port " << port << "\n";
    logToFile("System initialized at port " + configs["Port"]);

    //prepare first player slot
    players.emplace_back (new TcpSocket());
    players.back()->setBlocking (false);
    playerlist.emplace_back (static_cast<int>(players.size() - 1), "unknown", statusID::offline);
    playersTime.emplace_back();
    playersEnemy.emplace_back(-1);
    freeIndex = 0;
    debugPL;
    
    logClock.restart();
    while (working) {
        acceptNewPlayers();

        fromPlayer = 0;
        for (auto& player : players) {
            if (player->receive(packet) == Socket::Done) {
                packet >> toPlayer >> pktID;
                if (pktID != static_cast<int>(packetID::keepAlive)) 
                    {cout << "from: " << fromPlayer << " to: " << toPlayer << " id:" << packetIDnames[pktID] << "\n";}
                handleClientRequest(packet, pktID, fromPlayer, toPlayer);
            }
            ++fromPlayer;
        }
        checkTimeout();
        if (logClock.getElapsedTime().asSeconds() > 10) {
            logOnline();
            logClock.restart();
        }

        sleep(seconds(0.2));        
    }
}
