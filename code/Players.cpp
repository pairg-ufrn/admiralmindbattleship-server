void acceptNewPlayers() {
    //seek free player slot
    if  (playerlist[freeIndex].status != statusID::offline) {

        for (freeIndex = 0; freeIndex < players.size() and \
            playerlist[freeIndex].status != statusID::offline; \
            ++freeIndex);

        if (playerlist[freeIndex].status != statusID::offline) {
            players.emplace_back (new TcpSocket());
            players.back()->setBlocking (false);
            playerlist.emplace_back (static_cast<int>(players.size() - 1), "unknown", statusID::offline);
            playersTime.emplace_back();
            playersEnemy.emplace_back(-1);
            freeIndex = players.size()-1;
        }
    }

    if (listener.accept(*players[freeIndex]) == Socket::Done) {
        cout << "player " << freeIndex << " connected!\n";
        playerlist[freeIndex].status = statusID::unlogged;
        debugPL;
    }
}

void disconnectPlayer(size_t pl_id, bool timeout) {
    if (playerlist[pl_id].status == statusID::playing)
        sendGameEnd(pl_id, playersEnemy[pl_id]);

    if (playerlist[pl_id].status == statusID::playing or
        playerlist[pl_id].status == statusID::online)
        --playersOnline;

    playerlist[pl_id].status = statusID::offline;
    playersTime[pl_id].restart();
    playersEnemy[pl_id] = -1;
    players[pl_id]->disconnect();    
    freeIndex = pl_id;
    logToFile (playerlist[pl_id].name + " disconnected" + (timeout? "(time out)":""));
}

void checkTimeout() {
    bool changed = false;
    for (int i = 0; i < playersTime.size(); ++i) {
        if ((playersTime[i].getElapsedTime().asSeconds() >= timeout) and (playerlist[i].status != statusID::offline)) {
            disconnectPlayer(i, /*timeout=*/true);
            changed = true;
        }
    }
    if (changed)
        sendPlayerlist ();
}