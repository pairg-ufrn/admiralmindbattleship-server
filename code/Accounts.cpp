void loadAccounts () {
    ifstream input {"." + configs["DataDirectory"] + "users"};
    string id, user, pass, name, email, date, line;

    #define getword(_xyz_) getline(s, _xyz_, ';')

    if (input.is_open()) {
        while (getline (input, line)) {
            istringstream s{line};

            getword(id);
            getword(user);
            getword(pass);
            getword(name);
            getword(email);
            getword(date);

            accounts[user] = userAccount{id, user, pass, name, email, date};
        }
    } else {
        cout << ("." + configs["DataDirectory"] + "users") << " not found\n";
    }

    debugAccounts;
}

void saveAccounts () { //changed to order users by ID
    ofstream output {"." + configs["DataDirectory"] + "users"};
    vector<userAccount> accs(accounts.size());
    for (auto& userPair : accounts) {
        accs[stoi(userPair.second.id) - 1] = userPair.second;
    } 
    for (auto& user : accs) 
        output << user.id << ";" << user.user << ";" 
               << user.password << ";" << user.name <<";" 
               << user.email << ";"  << user.date << ";\n";
    output << std::flush;
}

void registerAccount (string ulogin, string upassword, string uname, string uemail) {
    string uid = to_string(accounts.size() + 1);
    accounts[ulogin] = userAccount{uid, ulogin, upassword, uname, uemail, getDatestamp()};
    saveAccounts();
    debugAccounts;
}