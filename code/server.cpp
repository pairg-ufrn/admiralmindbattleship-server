#include "SFML/Network.hpp"
#include "SFML/System.hpp"
#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <chrono>
#include <ctime>
#include "./GameDatatypes.hpp"

using namespace std;
using namespace sf;

#include "globals.hpp"
#include "functions.hpp"
#include "debug.hpp"

#include "Network.cpp"
#include "Players.cpp"
#include "Accounts.cpp"
#include "Util.cpp"
#include "Files.cpp"

int main () {
    start();
}