struct userAccount {
    string id, user, password, name, email, date;
};

TcpListener                       listener;
vector<unique_ptr<TcpSocket>>     players;
vector<sf::Clock>                 playersTime;
sf::Clock                         logClock;
vector<int>                       playersEnemy;
vector<playerInfo>                playerlist;           
map<string, userAccount>          accounts;
map<string, string>               configs;
size_t                            freeIndex;
size_t                            matchCount;
size_t                            playersOnline = 0;

constexpr int timeout = 30;