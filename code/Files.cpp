void logOnline () {
    ofstream output {"." + configs["LogDirectory"] + "online.log"};
    for (auto& pinfo : playerlist) {
        if (pinfo.status == statusID::online)
            output << pinfo.name << "\n";
        if (pinfo.status == statusID::playing)
            output << pinfo.name << " vs " << playerlist[playersEnemy[pinfo.id]].name << "\n";
    }
    output << std::flush;
}

void logToFile (string message) {
    ofstream output {"." + configs["LogDirectory"] + "admiralmind_" + getDatestamp('-') + ".log", ios::app};
    output << getTimestamp() << " " << message << "\n";
    output << std::flush;
}

void countMatches() {
    ifstream input {"." + configs["DataDirectory"] + "matches"};
    if (input.is_open()) {
        string line;
        while (getline(input, line)) {
            ++matchCount;
        }
    }
}

void saveMatch(string match) {
    //contar o numero de linhas para salvar ID...
    ofstream output {"." + configs["DataDirectory"] + "matches", ios::app};
    output << matchCount + 1 << ";" << match << "\n";
    ++matchCount;
    output << std::flush;
}

void loadConf() {
    configs["Port"] = "50986";
    configs["LogDirectory"] = "/log/";
    configs["DataDirectory"] = "/data/";
    ifstream input {"./admiralmindd.conf"};
    if (input.is_open()) {
        string line;
        vector<string> words;
        while (getline(input, line)) {
            if (not line.empty() and line[0] == '#') continue;
            words = splitIntoWords(line);
            if (words.size() > 1) {configs[words[0]] = words[1];}
        }
    };
    countMatches();
}